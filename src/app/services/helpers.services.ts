import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';


@Injectable()
export class HelpersServices {

  constructor(private _router: Router, private _toastyService: ToastrService) {
  }


  /**
   * Notificaiones de mensajes de error
   * @param {string} msg
   */
  public errorMsg(msg: string) {
    this._toastyService.error(msg);
  }

  /**
   * Notificacion de mensajes de exito
   * @param {string} msg
   */
  public success(msg: string) {
    this._toastyService.success(msg);
  }

}
