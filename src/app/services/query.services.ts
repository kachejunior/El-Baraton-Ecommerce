import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';

@Injectable()
export class QueryService {

  constructor(private _http: HttpClient) {
  }


  /**
   * Consultar productos del json, en caso de remplazar la url por la de una api que cumpla con el formato adecuado deberia funcionar, generalmente desde la api se realiza el filtrado.
   * @param callback
   * @param filter
   */
  getProducts(callback: any, filter:any = {orderBy:1}) {
    this._http.get('assets/test/products.json')
      .subscribe(data => {
        return callback(data, null);
      }, error => {
        return callback(null, error);
      });
  }

  /**
   * Consultar categorias del json, en caso de remplazar la url por la de una api que cumpla con el formato adecuado deberia funcionar.
   * @param callback
   * @param filter
   */
  getCategories(callback: any) {

    this._http.get('assets/test/categories.json')
      .subscribe(data => {
        return callback(data, null);
      }, error => {
        return callback(null, error);
      });
  }

}
