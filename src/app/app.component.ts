import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: '<menu-component></menu-component>' +
    '<router-outlet></router-outlet>' +
    '<toaster-container></toaster-container>',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
}
