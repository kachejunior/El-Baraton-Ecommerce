/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component, Input, OnInit} from '@angular/core';
import {HelpersServices} from './../../services';

@Component({
  selector: 'mini-component',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})

export class Mini implements OnInit {
  @Input() itemInput: any; // Ingreso desde componente padre
  public cantidad = 1; // Cantidad
  public style: any = {};
  public item = {
    quantity: 0,
    price: 0,
    available: true,
    sublevel_id: 0,
    name: '',
    id: '',
    img: '',
    link: ''
  };

  constructor(private _h: HelpersServices) {
  }

  /**
   * Inicializacion cuando llega de componente padre
   */
  ngOnInit() {
    this.item = this.itemInput;
    this.style = {
      'background': "url('"+this.item.img+"')",
      'background-size': 'cover',
      'background-position': 'center',
      ' background-repeat': 'no-repeat',
    };
  }

  /**
   * Validacion de cantidad
   */
  public validationNumber(): void {
    if (this.cantidad <= 0) {
      this.cantidad = 1;
    }
    if ((this.cantidad % 1) > 0) {
      this.cantidad = this.cantidad - (this.cantidad % 1);
    }
    if (this.cantidad > this.item.quantity) {
      this.cantidad = this.item.quantity;
    }
  }

  /**
   * Agregar 1 mas
   */
  public add() {
    if (this.cantidad < this.item.quantity) {
      this.cantidad++;
    }
    else {
      this._h.errorMsg('Maximo permitido: '+this.item.quantity);
    }
  }

  /**
   * Descartar 1 mas
   */
  public sub() {
    if (this.cantidad > 1) {
      this.cantidad--;
    }
    else {
      this._h.errorMsg('Se necesita al menos 1 producto para ser agregado al carrito');
    }
  }

  /**
   * Agregar a carrito
   */
  public addToCart() {
    if (localStorage.getItem('carrito')) {
      let list = JSON.parse(localStorage.getItem('carrito'));
      let control = false;
      for (let i = 0; i < list.length; i++) {
        if (list[i].id == this.item.id) {
          list[i].cantidad = list[i].cantidad + this.cantidad;
          if (list[i].cantidad > list[i].item.quantity) {
            this._h.errorMsg('Se permitira para este producto hasta: '+this.item.quantity);
            list[i].cantidad = list[i].item.quantity;
          }
          control = true;
        }
      }
      if (!control) {
        list.push({
          id: this.item.id,
          item: this.item,
          cantidad: this.cantidad
        });
      }
      localStorage.setItem('carrito', JSON.stringify(list));
    }
    else {
      let list = [];
      list.push({
        id: this.item.id,
        item: this.item,
        cantidad: this.cantidad
      });
      localStorage.setItem('carrito', JSON.stringify(list));
    }
     this._h.success('Agregado con exito');
    // this._floatService.addToCart(this.productoDefault.idProducto, this.cantidad);
  }

}
