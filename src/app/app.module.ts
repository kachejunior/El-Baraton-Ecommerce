import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {RouterModule, PreloadAllModules} from '@angular/router';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import { ToastrModule } from 'ngx-toastr';

import {AppComponent} from './app.component';
import {ROUTES} from './app.routes';

import {QueryService, HelpersServices} from './services';
import {HomeComponent} from './pages/home';
import {Menu, Mini} from './components';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Menu,
    Mini
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    ToasterModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(ROUTES, {useHash: false, preloadingStrategy: PreloadAllModules}),
  ],
  providers: [QueryService, HelpersServices, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
