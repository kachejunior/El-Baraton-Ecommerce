/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from '@angular/core';
import {QueryService, HelpersServices} from './../../services';
import * as _ from 'lodash';

@Component({
  selector: 'home-component',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})

export class HomeComponent {

  public listProducts: any = []; //Listado de productos base
  public orderBy: string = '1'; //Tipo de ordenamiento
  public search: string = ''; //Buscador
  public disponible: boolean = true; //Control de disponibilidad
  public priceMin: number = 0; //Precio min
  public priceMax: number = 500; //Precio maximo
  public qtyMin: number = 0; //Cantidad minima
  public qtyMax: number = 2000; //Cantidad Maxima
  public openCategory: boolean = false; //Control de menu de categorias
  //Categoria seleccionada
  public categorySelect: any = {
    id: 0,
    name: '',
    sublevels: []
  };
  public listCategory: any = []; //listado de categorias desde servicios
  public listCategoryMenu: any = []; //Listado de categorias para el menu en base a la opciones seleccionadas


  constructor(private  _q: QueryService, private _h: HelpersServices) {
    this.loadProduct();

    this._q.getCategories(
      (res, err) => {
        if (err) {
          this._h.errorMsg('Error en carga de categorias');
        }
        else {
          this.listCategory = res.categories;
          this.listCategoryMenu.push(_.cloneDeep(this.listCategory));
        }
      }
    );
  }

  /**
   * Carga de productos desde servicio y filtrado de los mismos (el modo correcto es desde una api pero en este caso se filtrara a nivel local)
   */
  public loadProduct() {
    this._q.getProducts(
      (res, err) => {
        if (err) {
          this._h.errorMsg('Error en carga de productos');
        }
        else {
          let keyOrder = 'price';
          let order = 'asc';
          switch (parseInt(this.orderBy)) {
            case 1: {
              keyOrder = 'price';
              order = 'asc';
              break;
            }
            case 2: {
              keyOrder = 'price';
              order = 'desc';
              break;
            }
            case 3: {
              keyOrder = 'quantity';
              order = 'desc';
              break;
            }
            case 4: {
              keyOrder = 'quantity';
              order = 'asc';
              break;
            }
            case 5: {
              keyOrder = 'available';
              order = 'desc';
              break;
            }
          }
          let list: any = _.orderBy(res.products, keyOrder, order);
          if (this.disponible) {
            list = _.remove(list,
              (n) => {
                return n.available;
              });
          }
          if (this.priceMin >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.price >= this.priceMin);
              });
          }
          if (this.priceMax >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.price <= this.priceMax);
              });
          }
          if (this.qtyMin >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.quantity >= this.qtyMin);
              });
          }
          if (this.qtyMax >= 0) {
            list = _.remove(list,
              (n) => {
                return (n.quantity <= this.qtyMax);
              });
          }
          if (this.search !== '') {
            list = _.remove(list,
              (n) => {
                return n.name.toLowerCase().includes(this.search.toLowerCase());
              });
          }
          if (this.categorySelect.id > 0) {
            list = _.remove(list,
              (n) => {
                return (n.sublevel_id == this.categorySelect.id);
              });
          }
          this.listProducts = list;
        }
      }
    );
  }

  /**
   * Seleccion de categoria
   * @param item
   * @param listOrder
   */
  public selectCategory(item, listOrder) {
    console.log(listOrder);
    if (item.sublevels) {
      let tam = this.listCategoryMenu.length - (listOrder + 1);
      this.listCategoryMenu = _.dropRight(this.listCategoryMenu, tam);
      this.listCategoryMenu.push(_.cloneDeep(item.sublevels));
    }
    else {
      this.categorySelect = item;
      console.log(this.categorySelect);
      this.openCategory = !this.openCategory;
      this.loadProduct();
    }
  }

  /**
   * Retornar a todos los productos y categorias
   */
  public todos() {
    this.categorySelect = {
      id: 0,
      name: '',
      sublevels: []
    };
    this.listCategoryMenu.length = 0;
    this.listCategoryMenu.push(_.cloneDeep(this.listCategory));
    this.loadProduct();
  }
}
